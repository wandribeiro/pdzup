# Projeto do PDZ

Proposta: Criar um cenário onde possa demonstrar minha evolução com cloudformation, provisionando recursos na AWS.

## Cenário esperado

- [x] 1 Banco de Dados - MySQL
- [x] 1 Bucket S3 (upload das imagens do site)
- [x] 1 Instancia EC2 t2.micro com ubuntu rodando uma aplicação phyton
- [ ] 1 Route 53 para resolver o domínioo

## Entregas extras

- [x] Security Group, liberando entrada e saída para qualquer IP
- [ ] Criar VPC, para não utilizar o default
- [ ] Criar outra instance para promover 'Load Balance'

## Prazo

+ Início - 15/09/2021
+ Término - 15/10/2021

### Lições aprendidas

- O template gerado e/ou no código fonte está 'setado' credenciais, precisa buscar uma solução para não armazenar isso.
- Security group sao essenciais para promover as permissões a nivel de rede nas aplicações.




